import React from "react";
import HomePageViewWrapper from "./HomePageView.Style";
import HomeContainer from "../Containers/HerosContainer";
import MarvelLogo from "../Imagens/MarvelLogo.svg.png";

const HomePageView = () => {
  return (
    <HomePageViewWrapper>
      <div className="home-page">
        <img className="marvel-logo" src={MarvelLogo} />
      </div>
      <div className="page">
        <HomeContainer />
      </div>
      <div className="rodape">
        <div className="texto-rodape">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua.
        </div>
      </div>
    </HomePageViewWrapper>
  );
};

export default HomePageView;
