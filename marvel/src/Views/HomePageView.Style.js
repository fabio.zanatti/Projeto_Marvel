import React from "react";
import styled from "styled-components";

const HomePageViewWrapper = styled.div`
  .page-inicial {
    background-color: black;
  }
  .home-page {
    width: 1440px;
    height: 95px;
    background-color: #ed1d24;
  }
  .marvel-logo {
    width: 240px;
    height: 95px;
  }
  .page {
    width: 1440px;
    height: 1100px;
    background-color: #f4f8fb;
    padding-left: 72px;
    padding-top: 50px;
  }

  .linha {
    width: 1440px;
    height: 1430px;
    background-color: #f4f8fb;
  }
  .rodape {
    width: 1440px;
    height: 222px;
    background-color: #edeef0;
    margin: auto;
    display: flex;
    justify-content: center;
    border-top: solid 9px;
    border-color: red;
  }
  .texto-rodape {
    width: 300px;
    height: 38px;
    font-family: Roboto;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    display: flex;
    font-stretch: normal;
    line-height: normal;
    -webkit-letter-spacing: normal;
    -moz-letter-spacing: normal;
    -ms-letter-spacing: normal;
    letter-spacing: normal;
    text-align: center;
    color: #4a4a4a;
    /* margin-top: 6px; */
    align-self: center;
    flex: 1;
    margin: auto;
    margin-left: 310px;
    padding-top: 114px;
  }
`;

export default HomePageViewWrapper;
