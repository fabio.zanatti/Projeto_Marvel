import React from "react";
import Card from "../Components/Card";
import { Pagination, Row, Col } from "antd";

const HeroesList = ({ list, pageAtual }) => (
  <Row type="flex">
    {list[pageAtual].map(item => (
      <Col key={item.id} span={6} className="quadrado">
        <Card 
          nome={item.name}
          descricao={item.description}
          img={`${item.thumbnail.path}.${item.thumbnail.extension}`}
        />
      </Col>
    ))}
  </Row>
);

export default HeroesList;
