import axios from "axios";

const URL =
  "https://gateway.marvel.com:443/v1/public/characters?ts=1&apikey=4a71eada596ef725078a1bfd59014aa1&hash=05be060364a4d3089860e60bfceecf67";

const initialState = {
  loadig: false,
  heroes: []
};

function buscaHeroesMarvelAction(state, action) {
  var numerosItens = 0;
  const num = Math.floor(action.payload.data.data.results.length/12);
  const listDivida = [];
  const list = action.payload.data.data.results;
  for (var i = 0; i < num; i++) {
    listDivida.push(list.slice(numerosItens, numerosItens + 12));
    numerosItens= numerosItens + 12;

    if(numerosItens + 12 > list.length) {
      listDivida.push(list.slice(numerosItens, list.length -1));
    }
  }
  return { ...state, heroes: listDivida };
}

export function heroesReducer(state = initialState, action) {
  switch (action.type) {
    case "HEROES_BUSCA":
      return buscaHeroesMarvelAction(state, action);
    default:
      return state;
  }
}

export function buscaHeroes() {
  return dispatch => {
    axios.get(URL).then(response => {
      dispatch({
        type: "HEROES_BUSCA",
        payload: response
      });
    });
  };
}
