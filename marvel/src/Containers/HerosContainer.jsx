import React from "react";
import { connect } from "react-redux";
import { Row, Col, Pagination } from "antd";
import Titulo from "../Components/Titulo";
import HomeWrapper from "./HerosContainer.Style";
import Filter from "../Components/Filter";
import { buscaHeroes } from "./Redux";
import HeroesList from "./HeroesList";

class HomeContainer extends React.PureComponent {
  constructor() {
    super();
    this.state = {
      pageAtual: 0
    };

    this.mudarPage = this.mudarPage.bind(this);
  }

  componentDidMount() {
    console.log("passei");
    this.props.buscaHeroes();
  }

  mudarPage(e) {
    console.log('e=>', e);
    this.setState({
      pageAtual: e -1
    });
  }

  render() {
    console.log("heroes", this.props.heroes);
    return (
      <HomeWrapper>
        <Row>
          <Col>
            <Titulo texto="Character" />
          </Col>
        </Row>
        <Row>
          <Col span={10} className="box-input">
            <input className="input-heroes" placeholder="Characters" />
          </Col>
          <Col span={6}>
            <Filter setaController={true} />
          </Col>
        </Row>
        <Row>
          {this.props.heroes.length > 0 && (
            <Col span={22}>
              <HeroesList
                list={this.props.heroes}
                pageAtual={this.state.pageAtual}
              />
            </Col>
          )}
        </Row>
        <Row type="flex" justify="center">
          <Col span={6}>
            <Pagination
              total={20}
              onChange={this.mudarPage}
            />
          </Col>
        </Row>
      </HomeWrapper>
    );
  }
}

const mapStateToProps = state => {
  console.log("state", state);
  return {
    heroes: state.heroesReducer.heroes
  };
};

export default connect(
  mapStateToProps,
  { buscaHeroes }
)(HomeContainer);
