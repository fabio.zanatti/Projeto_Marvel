import React from "react";
import styled from "styled-components";

const HomeWrapper = styled.div`
  .box-input {
    width: 393px;
    height: 91.3px;
    align-items: center;
    display: flex;

    .input-heroes {
      width: 385px;
      height: 51px;
      border-radius: 3px;
      background-color: #ffffff;
      border-color: gray;
      border-top: 0px;
      border-left: 0px;
      border-right: 0px;
    }
  }

  .list {
    display: flex;
  }
  .quadrado {
    margin-bottom: 25px;
  }
`;

export default HomeWrapper;
