import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import HomePageView from './Views/HomePageView';

class App extends Component {
  render() {
    return (
      <HomePageView />
    );
  }
}

export default App;
