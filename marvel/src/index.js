import "./index.css";
import React from "react";
import 'antd/dist/antd.css'
import ReactDOM from "react-dom";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux'
import reducer from './reducer';
import thunk from 'redux-thunk';
import promiseMiddleware from 'redux-promise';
import promise from 'redux-promise'

const createStoreWithMiddleware = applyMiddleware(
  promise,
  thunk
)(createStore);

const store = createStoreWithMiddleware(reducer);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById("root")
);
registerServiceWorker();