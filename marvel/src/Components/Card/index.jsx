import React from "react";
import CardWrapper from "./Card.Style";

const Card = ({ img, nome, nomeReal, descricao }) => (
  <CardWrapper>
    <div className="Rectangle">
      <img className="img-heroes" src={img} />
      <div className="nome-heroes ">{nome}</div>
      <div className="nome-real">{nomeReal}</div>
      <div className="description ">{descricao}</div>
      <div className="Rectangle2" />
    </div>
  </CardWrapper>
);

export default Card;
