import React from "react";
import styled from "styled-components";

const CardWrapper = styled.div`
  .Rectangle {
    width: 290px;
    height: 220px;
    border-radius: 8px;
    background-color: #ffffff;
    border: solid 1px #d8dde6;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .nome-heroes {
    width: 270px;
    height: 23px;
    font-family: Roboto;
    font-size: 16px;
    font-weight: 500;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.44;
    letter-spacing: normal;
    text-align: center;
    color: #4a4a4a;
  }

  .nome-real {
    width: 248px;
    height: 23px;
    font-family: Roboto;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.64;
    letter-spacing: normal;
    text-align: center;
    color: #ed1d24;
  }
  
  .img-heroes {
    width: 56px;
    height: 56px;
    display: flex;
    border-radius: 32px;
    margin: auto;
    margin-top: 14px;
}
  }

  .description {
    width: 255px;
    height: 92px;
    font-family: Roboto;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: 1.64;
    padding-left: 29px;
    -webkit-letter-spacing: normal;
    -moz-letter-spacing: normal;
    -ms-letter-spacing: normal;
    -webkit-letter-spacing: normal;
    -moz-letter-spacing: normal;
    -ms-letter-spacing: normal;
    letter-spacing: normal;
    text-align: justify;
    color: #4a4a4a;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .Rectangle2{
    margin-top: -6px;
    width: 290px;
    height: 16px;
    border-radius: 8px;
    border-bottom: solid 3px #ed1d24;
    background-color: #ffffff;
    z-index: 999;
    display: block;
  }
`;

export default CardWrapper;
