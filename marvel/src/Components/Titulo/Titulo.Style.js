import React from "react";
import styled from "styled-components";

const TituloWrapper = styled.div`
  width: 431px;
  height: 52px;
  font-family: Roboto-Light;
  font-size: 36px;
  font-weight: 300;
  font-style: normal;
  font-stretch: normal;
  line-height: 1.61;
  letter-spacing: normal;
  text-align: left;
  color: #ed1d24;
`;

export default TituloWrapper;
