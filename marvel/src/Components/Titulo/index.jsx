import React from "react";
import TituloWrapper from "./Titulo.Style";

const Titulo = ({ texto }) => <TituloWrapper>{texto}</TituloWrapper>;

export default Titulo;
