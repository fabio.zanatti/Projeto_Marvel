import React from "react";
import setaCima from "../../Imagens/arrow-up.svg";
import filter from "../../Imagens/filter.svg";
import setaBaixo from "../../Imagens/arrow-down.svg";
import FilterWrapper from "./Filter.Style";

const Filter = ({ setaController }) => (
  <FilterWrapper>
    <img className="filter" src={filter} />
    <div className="label">A-Z</div>
    <img src={setaController ? setaCima : setaBaixo} className="seta" />
  </FilterWrapper>
);

export default Filter;
