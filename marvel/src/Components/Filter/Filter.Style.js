import React from "react";
import styled from "styled-components";

const FilterWrapper = styled.div`
  width: 393px;
  height: 91.3px;
  align-items: center;
  display: flex;
  .filter {
    width: 24px;
    height: 24px;
    object-fit: contain;
  }

  .label {
    width: 46px;
    height: 11px;
    font-family: Roboto;
    font-size: 14px;
    font-weight: normal;
    font-style: normal;
    font-stretch: normal;
    line-height: normal;
    -webkit-letter-spacing: normal;
    -moz-letter-spacing: normal;
    -ms-letter-spacing: normal;
    letter-spacing: normal;
    text-align: left;
    color: #4a4a4a;
    margin-left: 10px;
  }

  .seta {
    width: 19px;
    height: 54.2px;
    color: #c6c8d4;
  }
`;

export default FilterWrapper;
