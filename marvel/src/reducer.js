import { combineReducers } from "redux";
import { heroesReducer } from "./Containers/Redux";

export default combineReducers({
  heroesReducer
});
